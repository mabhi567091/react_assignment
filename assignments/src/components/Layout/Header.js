import { Fragment } from 'react';
import style from './Header.module.css';
import MealsImg from './assets/meals.jpg';
import HeaderCartButton from './HeaderCartButton';

const Header = (props) => {
  return (
    <Fragment>
      <header className={style.header}>
        <h1>ReactMeals</h1>
        <HeaderCartButton />
      </header>
      <div className={style['main-image']}>
        <img src={MealsImg} alt='Delicious Food' />
      </div>
    </Fragment>
  );
};
export default Header;
