import { useEffect } from 'react';
import { useState } from 'react';

// useEffect is a function which react provides to us
// to access the lifecycle events
// accepts 2 parameters
// 1. callback fn, lifecycle listener
// 2. optional --> dependency array
//    depending on the dependency array the callback is called

// 1. NO dependency array passed
//    --> call the callback everytime the component updates & also the first time

// 2. we pass an dependency array
//    --> whenever the value mentioned in the dependency array changes
//    --> callback is called

// 3. if the useEffect which has empty dependency array
// returns a function, that function is going to be called
// just before the component is destroyed

const FunctionalLifecycle = () => {
  const [count, setCount] = useState(0);
  const [anotherCount, setOtherCount] = useState(10);

  const incrementCount = () => setCount(count + 1);
  const incrementOtherCount = () => setOtherCount(anotherCount + 1);

  // componentDidUpdate
  useEffect(() => {
    console.log('COMPONENT RENDERED [for the 1st time] OR UPDATED');

    // not correct
    // return () => console.log('DESTROYING COMPONENT');
  });

  // equivalent to componentDidMount
  useEffect(() => {
    console.log('THE FIRST TIME THE COMPONENT RENDERS!');

    // equivalent to componentWillUnmount
    return () => console.log('DESTROYING COMPONENT');
  }, []);

  // componentDidUpdate (only for specific variables)
  useEffect(() => {
    console.log('anotherCount updated & the value is ', anotherCount);

    // not correct
    // return () => console.log('DESTROYING COMPONENT');
  }, [anotherCount]);

  // -- render method --
  return (
    <div>
      <span>{count}</span>
      <span> {anotherCount} </span>
      <button onClick={incrementCount}>update count</button>
      <button onClick={incrementOtherCount}>update other count</button>
    </div>
  );
};

export default FunctionalLifecycle;
