import { Component } from 'react';

class LifeCycle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      count: 0,
    };
  }

  // lifecycle method --> called by react
  // the first time this component is rendered to the screen
  // mount === rendering
  componentDidMount() {
    console.log('COMPONENT WAS RENDERED FOR THE FIRST TIME');

    setTimeout(() => {
      this.setState({ count: 2 });
    }, 5000);
  }

  // lifecycle method --> called by react
  // everytime the render method is called as a result of an update
  // in the state or the props
  componentDidUpdate() {
    console.log('UPDATED THE COMPONENT');
  }

  // lifecycle method --> called by react
  // just before the component is destroyed
  componentWillUnmount() {
    console.log('COMPONENT ABOUT TO GET DESTROYED');
  }

  // called everytime the "state" or "props" change
  // because we want our UI to reflect the new state/props
  render() {
    console.log('RENDERING...');

    return (
      <div>
        <span>{this.state.count}</span>
      </div>
    );
  }
}

export default LifeCycle;
